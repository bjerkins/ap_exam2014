% --------------------------------------------------------------------

%                              SHEET

%                        31st October 2014

%                       Bjarki Madsen(lch929)       
%                     University of Copenhagen

% --------------------------------------------------------------------
-module(sheet).

%% API
-export([ sheet/0
        , cell/2
        , add_viewer/2
        , remove_viewer/2
        , get_viewers/1
        , set_value/2
        , shut_down/1]).

%%%===================================================================
%%% API
%%%===================================================================

sheet() ->
  init_sheet().

cell(S, A) ->
  init_cell(S, A).

% since there might be a reqeuest for this viewer later, this function
% should be blocking and process continue when the viewer has been
% added
add_viewer(C, P) ->
  Req = {add_viewer, P},
  rpc(C, Req).

remove_viewer (C, P) ->
  Req = {remove_viewer, P},
  async(C, Req).

get_viewers(C) ->
  rpc(C, get_viewers).

set_value(C, V) ->
  Req = {set_value, V},
  async(C, Req).

shut_down(S) ->
  S ! stop.

%%%===================================================================
%%% Internal functions
%%%===================================================================

init_sheet() ->
  % Sheet server contains
  %   - mapping from name to cell servers
  Server = spawn(fun() -> sheet_loop([]) end),
  {ok, Server}.

init_cell(Sheet, A) ->
  % Cell server contains (in the order of the object to cell_loop)
  %   - Name
  %   - Sheet Pid
  %   - List of viewers
  %   - Mapping dependencies to values
  %   - Value
  %   - Formula
  Cell = spawn(fun() -> cell_loop({A, Sheet, [], [], undefined, undefined}) end),
  register(A, Cell),
  async(Sheet, {new_cell, A, Cell}),
  {ok, Cell}.


%%% Communication primitives

async(Pid, Msg) ->
  Pid ! Msg.

rpc(Pid, Request) ->
  Pid ! {self(), Request},
  receive
    {Pid, Response} ->
      Response
  end.

reply(From, Msg) ->
  From ! {self(), Msg}.


%%% Server loops

sheet_loop(CellMap) ->
  receive
    stop ->
      io:format("Sheet ~p stopping~n", [self()]),
      lists:foreach(fun({_, Pid}) -> Pid ! stop end, CellMap),
      ok;
    {new_cell, Name, Pid} ->
      % first, check if there are other cells that depend on this one
      lists:foreach(fun ({_, P}) ->
        Dps = rpc(P, get_depends),
        % io:format("Cell ~p created, checking for viewers: ~p~n", [Name, Dps]),
        case lists:member(Name, Dps) of
          false -> nuttin; % doesnt have dependency
          true -> % it does have dependecy ! add ourselves as viewer
            % io:format("Cell ~p has dep to ~p~n", [P, Name]),
            rpc(Pid, {add_viewer, P})
        end end, CellMap),
      sheet_loop([{Name, Pid} | CellMap])
  end.

cell_loop(Cell) ->
  receive
    stop ->
      {_, S, _, _, _, _} = Cell,
      io:format("Cell ~p stopping in Sheet ~p~n", [self(), S]),
      ok;
    {From, {add_viewer, P}} ->
      {N, S, Vs, Dps, V, F} = Cell,
      % let our viewer know our value, if any
      % io:format("Cell ~p with value ~p adding viewer ~p~n", [N, V, P]),
      case V of 
        undefined -> nuttin;
        _ -> async(P, {updated, N, {def, V}, nuttin})
      end,
      reply(From, ok),
      cell_loop({N, S, [P | Vs], Dps, V, F});
    {From, get_value} ->
      {_, _, _, _, V, _} = Cell,
      reply(From, V),
      cell_loop(Cell);
    {From, get_viewers} ->
      {_, _, Vs, _, _, _} = Cell,
      reply(From, Vs),
      cell_loop(Cell);
    {From, get_depends} ->
      {_, _, _, Dps, _, _} = Cell,
      reply(From, Dps),
      cell_loop(Cell);
    {remove_viewer, P} ->
      {N, S, Vs, Dps, V, F} = Cell,
      cell_loop({N, S, lists:delete(P, Vs), Dps, V, F});
    {set_value, Value} ->
      {N, S, Vs, Dps, V, F} = Cell,
      case Value of 
        {formula, Formula, Deps} ->
          % add our self as viewer to Deps if they are defined
          lists:foreach(fun (Dep) ->
            Maybz = whereis(Dep),
            case Maybz of
              undefined -> nuttin; % cant do nuttin', will be added later when created
              _ -> % it's a process!
                async(Maybz, {add_viewer, self()})
            end end, Deps),
          cell_loop({N, S, Vs, Deps, V, Formula});
        _ ->  
          % send to all viewers
          lists:foreach(fun (Viewer) ->
            % io:format("Got value ~p, sending to ~p~n", [Value, Viewer]),
            async(Viewer, {updated, N, {def, Value}, nuttin}) end, Vs),
          cell_loop({N, S, Vs, Dps, Value, F})
      end;
    {updated, Name, {def, Any}, Extra} ->
      {N, S, Vs, Dps, V, F} = Cell,
      % io:format("Cell ~p just got value ~p sent~n", [N, Any]),
      % check if all deps have value
      Vals = deps_values(Dps),
      % io:format("Dep values ~p~n", [Vals]),
      case length(Vals) == length(Dps) of
        false -> % some cells that still need values
          cell_loop(Cell);
        true -> % calculate our value!
          NewValue = F(Vals),
          % io:format("All deps have values. My value is ~p~n", [NewValue]),
          % let everyone know of our new value!
          lists:foreach(fun(Viewer) ->
            async(Viewer, {updated, N, {def, NewValue}, nutting}) end, Vs),
          % finally, update ourselves and sleep, been a long day
          cell_loop({N, S, Vs, Dps, NewValue, F})
      end;
    {updated, N, undefined, Extra} ->
      cell_loop(Cell);
    {updated, N, updating, Extra} ->
      cell_loop(Cell);
    {updated, N, error, Extra} ->
      cell_loop(Cell)
  end.

%%% Helping functions

deps_values([]) -> [];
deps_values([Dep | Deps]) ->
  Maybz = whereis(Dep),
  case Maybz of
    undefined -> 
      [];
    _ ->
      PossValue = rpc(Maybz, get_value),
      % io:format("Value ~p~n", [PossValue]),
      case PossValue of
        undefined -> [];
        V -> [V] ++ deps_values(Deps)
      end
  end.







