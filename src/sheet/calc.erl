-module(calc).

-export([len/0]).

dummy_viewer() ->
  receive
    stop ->
      ok
  end.

len() ->
  % spawn a server
  {_, S} = sheet:sheet(),
  % spawn a dummy viewer
  V = spawn(fun dummy_viewer/0),
  % spawn some cell
  {ok, A1} = sheet:cell(S, a),
  sheet:add_viewer(A1, V),
  L = sheet:get_viewers(A1),
  L.