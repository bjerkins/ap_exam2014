-module(sum).

-export([sum2/0]).

print_viewer() ->
    receive
        {updated, Name, {def, Val}, _} ->
            io:format("~p has value ~p~n", [Name, Val]),
            print_viewer();
        _ -> print_viewer()
    end.
    
sum2() ->
    {ok, Sheet} = sheet:sheet(),
    UI = spawn(fun print_viewer/0),
    {ok, Sum} = sheet:cell(Sheet, sum),
    sheet:add_viewer(Sum, UI),
    sheet:set_value(Sum, {formula, fun lists:sum/1, [a, b]}),
    {ok, A} = sheet:cell(Sheet, a),
    {ok, B} = sheet:cell(Sheet, b),
    sheet:set_value(A, 23),
    sheet:set_value(B, 19),
    {Sheet, Sum, A, B, UI}.