-- module FastParser
--        ( Error
--        , parseString
--        , parseFile
--        )
--        where
module FastParser where

import FastAST
import Control.Applicative((*>), (<$>))
-- Parsec includes
import Text.Parsec.Prim
import Text.Parsec.Char
import Text.Parsec.Error
import Text.Parsec.String
import Text.Parsec.Combinator

-- | You may change this type to whatever you want - just make sure it
-- is an instance of 'Show'.
type Error = ParseError

-- list of reserved keywords
keywords :: [String]
keywords = ["self", "class", "new", "receive", "send", 
            "match", "return", "set"]

-- 
-- runners
--
parseString :: String -> Either Error Prog
parseString s = case parse program "" s of
                     Left err -> Left err
                     Right res -> Right res 

parseFile :: FilePath -> IO (Either Error Prog)
parseFile f = parseString <$> readFile f

--
-- parsers
--

program :: Parser Prog
program = many classdecl

classdecl :: Parser ClassDecl
classdecl = do _ <- sToken "class"
               n <- name
               _ <- cToken '{'
               cd <- constrdecl
               nds <- nmethdecls
               rd <- recvdecl
               _ <- cToken '}'
               return $ ClassDecl { className = n,
                                    classConstructor = cd,
                                    classMethods = nds,
                                    classReceive = rd }

constrdecl :: Parser (Maybe ConstructorDecl)
constrdecl = (do _ <- sToken "new"
                 _ <- cToken '('
                 ps <- params
                 _ <- cToken ')'
                 _ <- cToken '{'
                 es <- exprs
                 _ <- cToken '}'
                 return $ Just MethodDecl { methodParameters = ps,
                                            methodBody = es}) <|>
              return Nothing

nmethdecls :: Parser [NamedMethodDecl]
nmethdecls = many nmethdecl

nmethdecl :: Parser NamedMethodDecl
nmethdecl = do n <- name
               _ <- cToken '('
               ps <- params
               _ <- cToken ')'
               _ <- cToken '{'
               es <- exprs
               _ <- cToken '}'
               let mdecl = MethodDecl { methodParameters = ps,
                                        methodBody = es} 
                in return $ NamedMethodDecl n mdecl

recvdecl :: Parser (Maybe ReceiveDecl)
recvdecl = (do _ <- sToken "receive"
               _ <- cToken '('
               p <- param
               _ <- cToken ')'
               _ <- cToken '{'
               es <- exprs
               _ <- cToken '}'
               return $ Just ReceiveDecl { receiveParam = p, 
                                           receiveBody = es }) <|>
            return Nothing

params :: Parser [Name]
params = (do p <- param
             paramsOpt [p]) <|>
          return []

paramsOpt :: [Name] -> Parser [Name]
paramsOpt ps = (do _ <- cToken ','
                   p <- param
                   paramsOpt $ ps ++ [p]) <|>
               return ps

param :: Parser Name
param = do n <- name
           return $ n

args :: Parser [Expr]
args = (do e <- exprEntry
           argsOpt [e]) <|>
       return []

argsOpt es = (do _ <- cToken ','
                 e <- exprEntry
                 argsOpt $ es ++ [e]) <|>
              return es

exprs :: Parser Exprs
exprs = (do e <- exprEntry
            exprsOpt [e]) <|>
         return []

exprsOpt :: Exprs -> Parser Exprs
exprsOpt es = try (do _ <- cToken ';'
                      e <- exprEntry
                      exprsOpt $ es ++ [e]) <|>
              (do _ <- cToken ';' 
                  return es)

exprEntry :: Parser Expr
exprEntry = do e <- expr
               exprOpt e

exprOpt :: Expr -> Parser Expr
exprOpt e = (do _ <- cToken '+'
                e2 <- expr
                exprOpt $ Plus e e2) <|>
            (do _ <- cToken '-'
                e2 <- expr
                exprOpt $ Minus e e2) <|>
            (do _ <- cToken '*'
                e2 <- expr
                exprOpt $ Times e e2) <|>
            (do _ <- cToken '/'
                e2 <- expr
                exprOpt $ DividedBy e e2) <|>
            (do _ <- cToken '.'
                n <- name
                _ <- cToken '('
                as <- args
                _ <- cToken ')'
                exprOpt $ CallMethod e n as) <|> 
            return e

expr :: Parser Expr
expr = (do i <- integer
           return $ IntConst i) <|>
       (do s <- qstring
           return $ StringConst s) <|>
       try (do n <- name
               _ <- cToken '('
               as <- args
               _ <- cToken ')'
               return $ TermLiteral n as) <|>
       try (do n <- name
               return $ ReadVar n) <|>
       try (do _ <- sToken "self"
               return $ Self) <|>
       (do _ <- sToken "return"
           e <- exprEntry
           return $ Return e) <|>
       try (do _ <- sToken "set"
               _ <- sToken "self"
               _ <- cToken '.'
               n <- name
               _ <- cToken '='
               e <- exprEntry
               return $ SetField n e) <|>
       try (do _ <- sToken "set"
               n <- name
               _ <- cToken '='
               e <- exprEntry
               return $ SetVar n e) <|>
       (do _ <- sToken "match"
           e <- exprEntry
           _ <- cToken '{'
           cs <- cases
           _ <- cToken '}'
           return $ Match e cs) <|>
       (do _ <- sToken "send"
           _ <- cToken '('
           e1 <- exprEntry
           _ <- cToken ','
           e2 <- exprEntry
           _ <- cToken ')'
           return $ SendMessage e1 e2) <|>
       try (do _ <- sToken "self"
               _ <- cToken '.'
               n <- name
               return $ ReadField n) <|>
       (do _ <- sToken "new"
           n <- name
           _ <- cToken '('
           as <- args
           _ <- cToken ')'
           return $ New n as) <|>
       (do _ <- cToken '('
           e <- exprEntry
           _ <- cToken ')'
           return e)

cases :: Parser Cases
cases = many f_case

-- 'f' for Fast
f_case :: Parser Case
f_case = do p <- pattern
            _ <- sToken "->"
            _ <- cToken '{'
            es <- exprs
            _ <- cToken '}'
            return (p, es)

pattern :: Parser Pattern
pattern = (do i <- integer
              return $ ConstInt i) <|>
          (do s <- qstring
              return $ ConstString s) <|>
          try (do n <- name
                  _ <- cToken '('
                  pms <- params
                  _ <- cToken ')'
                  return $ TermPattern n pms) <|>
          (do n <- name
              return $ AnyValue n)

-- quoted string
qstring :: Parser String
qstring = do q1 <- char '"'
             m  <- many1 letter
             q2 <- char '"'
             return $ [q1] ++ m ++ [q2]

integer :: Parser Integer
integer = do n <- (spaces *> many1 digit)
             return $ read n

name :: Parser Name
name = do first <- letter
          rest <- many $ letter <|> digit <|> char '_'
          let whole = first:rest in if whole `elem` keywords
                                      then parserZero
                                      else return whole
-- 
-- helpers
--

-- eliminates spaces when parsing characters
cToken :: Char -> Parser String
cToken c = try (many space >> char c >> many space)

-- eliminates spaces when parsing strings
sToken :: String -> Parser String
sToken s = try (many space >> string s >> many space)