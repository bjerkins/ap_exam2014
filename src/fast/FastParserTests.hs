import FastAST
import FastParser

import Test.HUnit

import Text.Parsec.Prim
import Text.Parsec.Char
import Text.Parsec.Error
import Text.Parsec.String
import Text.Parsec.Combinator

parsePass :: (Eq a) => Parser a -> String -> a -> Bool
parsePass p s t = case parse p "" s of
                    Right x -> x == t
                    Left _  -> False

parseFail :: Parser p -> String -> Bool
parseFail p s = case parse p "" s of
  Right _ -> False
  Left _ -> True


--------------- Integer ---------------


int_test1 = TestCase 
  $ assertBool "1. testing int" 
  $ parsePass integer "123" 123

int_test2 = TestCase 
  $ assertBool "2. testing int with spaces" 
  $ parsePass integer "123    " 123

int_test3 = TestCase
  $ assertBool "3. testing int with leading zero"
  $ parsePass integer "0123" 123


--------------- QString ---------------


qstring_test1 = TestCase
  $ assertBool "1. testing comment"
  $ parsePass qstring "\"test\"" "\"test\""


--------------- Name ---------------


name_test1 = TestCase
  $ assertBool "1. testing name"
  $ parsePass name "asdf" "asdf"

name_test2 = TestCase
  $ assertBool "2. testing illegal name"
  $ parseFail name "5asdf"

name_test3 = TestCase
  $ assertBool "3. testing reserved name"
  $ parseFail name "return"


------------- Pattern --------------


pttrn_test1 = TestCase
  $ assertBool "1. testing pattern int"
  $ parsePass pattern "123" (ConstInt 123)

pttrn_test2 = TestCase
  $ assertBool "2. testing pattern string"
  $ parsePass pattern "\"asdf\"" (ConstString "\"asdf\"")

pttrn_test3 = TestCase
  $ assertBool "3. testing pattern string"
  $ parsePass pattern "asdf (foo, bar)" (TermPattern "asdf" ["foo", "bar"])

pttrn_test4 = TestCase
  $ assertBool "4. testing pattern string"
  $ parsePass pattern "asdf ()" (TermPattern "asdf" [])

pttrn_test5 = TestCase
  $ assertBool "5. testing pattern string"
  $ parsePass pattern "asdf" (AnyValue "asdf")


------------- Expr ----------------


expr_test1 = TestCase
  $ assertBool "1. testing expression"
  $ parsePass exprEntry "5 + 5" (Plus (IntConst 5) (IntConst 5))

expr_test2 = TestCase
  $ assertBool "2. testing expression"
  $ parsePass exprEntry "5 - 5" (Minus (IntConst 5) (IntConst 5))

expr_test3 = TestCase
  $ assertBool "3. testing expression"
  $ parsePass exprEntry "5 * 5" (Times (IntConst 5) (IntConst 5))

expr_test4 = TestCase
  $ assertBool "4. testing expression"
  $ parsePass exprEntry "5 / 5" (DividedBy (IntConst 5) (IntConst 5))

expr_test5 = TestCase
  $ assertBool "5. testing expression"
  $ parsePass exprEntry "\"asdf\"" (StringConst "\"asdf\"")

expr_test6 = TestCase
  $ assertBool "6. testing expression"
  $ parsePass exprEntry "self" Self

expr_test7 = TestCase
  $ assertBool "7. testing expression"
  $ parsePass exprEntry "return 5"
                        (Return (IntConst 5))
expr_test8 = TestCase
  $ assertBool "8. testing expression"
  $ parsePass exprEntry "return (5 + 5)"
                        (Return (Plus (IntConst 5) (IntConst 5)))

expr_test9 = TestCase
  $ assertBool "9. testing expression"
  $ parsePass exprEntry "set self.asdf = 5"
                        (SetField "asdf" (IntConst 5))

expr_test10 = TestCase
  $ assertBool "10. testing expression"
  $ parsePass exprEntry "set self.asdf = (5+5)"
                        (SetField "asdf" (Plus (IntConst 5) (IntConst 5)))

expr_test11 = TestCase
  $ assertBool "11. testing expression"
  $ parsePass exprEntry "set asdf = (5 / 5)"
                        (SetVar "asdf" (DividedBy (IntConst 5) (IntConst 5)))

expr_test12 = TestCase
  $ assertBool "12. testing expression"
  $ parsePass exprEntry "send (self, 5)"
                        (SendMessage (Self) (IntConst 5))

expr_test13 = TestCase
  $ assertBool "13. testing expression"
  $ parsePass exprEntry "self.asdf (5, 10)"
                        (CallMethod Self "asdf" [(IntConst 5), (IntConst 10)])

expr_test14 = TestCase
  $ assertBool "14. testing expression"
  $ parsePass exprEntry "new asdf (5, 5)"
                         (New "asdf" [(IntConst 5), (IntConst 5)])

expr_test15 = TestCase
  $ assertBool "15. testing expression"
  $ parsePass exprEntry "new asdf (5, \"foo\")"
                        (New "asdf" [(IntConst 5), (StringConst "\"foo\"")])

expr_test16 = TestCase
  $ assertBool "16. testing expression"
  $ parsePass exprEntry "match n { 0 -> { return 1; } }"
                        (Match (ReadVar "n") [(ConstInt 0,[Return (IntConst 1)])])

expr_test17 = TestCase
  $ assertBool "17. testing expression"
  $ parsePass exprEntry "f.fact(0).printLn()"
                        (CallMethod (CallMethod (ReadVar "f") "fact" [IntConst 0]) "printLn" [])

expr_test18 = TestCase
  $ assertBool "18. testing expression TermLiteral"
  $ parsePass exprEntry "asdf(5, 3)"
                        (TermLiteral "asdf" [(IntConst 5), (IntConst 3)])

expr_test19 = TestCase
  $ assertBool "19. testing expression TermLiteral"
  $ parsePass exprEntry "asdf(foo(5), self.bar)"
                        (TermLiteral "asdf" [(TermLiteral "foo" [IntConst 5]), (ReadField "bar")])

expr_test20 = TestCase
  $ assertBool "20. testing expression TermLiteral"
  $ parsePass exprEntry "asdf(5, self.bar)"
                        (TermLiteral "asdf" [(IntConst 5), (ReadField "bar")])

expr_test21 = TestCase
  $ assertBool "21. testing expression"
  $ parsePass exprEntry "set self.fleh = asdf(foo(5), 3)"
                        (SetField "fleh" (TermLiteral "asdf" [(TermLiteral "foo" [IntConst 5]), IntConst 3]))



------------- Case ----------------

case_test1 = TestCase
  $ assertBool "1. testing case"
  $ parsePass f_case "asdf -> {5; 3;}"
                     ((AnyValue "asdf"), [(IntConst 5), (IntConst 3)])

------------- ReceiveDecl ----------------

recvdecl_test1 = TestCase
  $ assertBool "1. testing recvdecl"
  $ parsePass recvdecl "receive (asdf) { 5; }"
                       (Just (ReceiveDecl { receiveParam = "asdf", 
                                            receiveBody = [(IntConst 5)] }))

------------- NamedMethodDecl ----------------

nmethdecl_test1 = TestCase
  $ assertBool "1. testing nmethdecl"
  $ parsePass nmethdecl "n (foo, bar) { 5; 1; }"
                        (NamedMethodDecl "n" (MethodDecl { methodParameters = ["foo", "bar"],
                                                           methodBody = [(IntConst 5), (IntConst 1)]}))

------------- ConstructorDecl ----------------

constrdecl_test1 = TestCase
  $ assertBool "1. testing constrdecl"
  $ parsePass constrdecl "new (foo, bar) { 5 + 5; }"
                         (Just (MethodDecl { methodParameters = ["foo", "bar"],
                                             methodBody = [(Plus (IntConst 5) (IntConst 5))] }))

------------- ClassDecl ----------------
-- classdecl_test1 = TestCase
--   $ assertBool "1. testing classdecl"
--   $ parsePass classdecl "class Foo { new (bar) { 55 }; n (zar) {5; 1}; receive (asdf) {5}; }"

tests = TestList [TestLabel "running all the tests!"
  $ TestList [
    int_test1,
    int_test2,
    int_test3,
    qstring_test1,
    name_test1,
    name_test2,
    name_test3,
    pttrn_test1,
    pttrn_test2,
    pttrn_test3,
    pttrn_test4,
    pttrn_test5,
    expr_test1,
    expr_test2,
    expr_test3,
    expr_test4,
    expr_test5,
    expr_test6,
    expr_test7,
    expr_test8,
    expr_test9,
    expr_test10,
    expr_test11,
    expr_test12,
    expr_test13,
    expr_test14,
    expr_test15,
    expr_test16,
    expr_test17,
    expr_test18,
    expr_test19,
    expr_test20,
    expr_test21,
    case_test1,
    recvdecl_test1,
    nmethdecl_test1,
    constrdecl_test1
  ]]

-- TODO TEST: TermLiteral, Match

run = runTestTT tests



